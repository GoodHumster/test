//
//  CoreResourcesProvider.m
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreResourcesProvider.h"
#import "SoulRegistratedModule.h"
#import "SoulBaseModule.h"

#import <objc/runtime.h>

@interface CoreResourcesProvider ()

@end


@implementation CoreResourcesProvider

+(CoreResourcesProvider *)sharedInstance
{
    static CoreResourcesProvider* _sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[CoreResourcesProvider alloc] init];
    });
    
    return _sharedInstance;
}


-(id) provideResourceForViewModel:(id)viewModel
{
    NSArray *classes = [SoulRegistratedModule classArray];
    
    for  (Class<SoulBaseModule> class in classes)
    {
        Class viewModelClass = [class soulViewModelClass];
        if ([viewModel isKindOfClass:viewModelClass])
        {
            return [class initWithViewModel:viewModel];
        }
    }
    
    return nil;
}

@end