//
//  SportDescriptionViewController.m
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SportDescriptionViewController.h"
#import "SportDescriptionViewModel.h"
#import "SoulRegistratedModule.h"


@interface SportDescriptionViewController()

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UITextView *userMarks;
@property (weak, nonatomic) IBOutlet UIButton *back;

@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topName;

@property (nonatomic, strong) SportDescriptionViewModel *viewModel;

@end


@implementation SportDescriptionViewController

#pragma mark - init methods

+(void) load
{
    [SoulRegistratedModule registeratedClass:[SportDescriptionViewController class]];
}

-(instancetype) initWithViewModel:(id)viewModel
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowed:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    
    return self;
}


-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationView.layer.masksToBounds = NO;
    self.navigationView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.25].CGColor;
    self.navigationView.layer.shadowOffset = CGSizeMake(0, 5);
    self.navigationView.layer.shadowOpacity = 0.5;
    self.navigationView.layer.shadowRadius = 1;
    
    id<Competition> competition = [self.viewModel getCompletition];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[competition startInSeconds]];
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    self.name.text = [competition name];
    self.startTime.text =  [dateFormmater stringFromDate:date];
    self.duration.text = [NSString stringWithFormat:@"%d min",(int)([competition durationInSeconds]/60)];
    self.userMarks.text = [competition userMark];
    [self bindViewModel];
}


-(void) bindViewModel
{
    RAC(self.userMarks,text) = [self.userMarks.rac_textSignal map:^id(id value) {
        [self.viewModel updateUserMark:value];
        return value;
    }];
    
    self.back.rac_command = self.viewModel.backCommand;
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - notification nandler

-(void) keyboardWillHide:(NSNotification *)notif
{
    self.bottomTextView.constant = 0;
    self.topName.constant = 15;
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void) keyboardWillShowed:(NSNotification *)notif
{
    NSDictionary *info = notif.userInfo;
    CGRect keyboardFrame = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.bottomTextView.constant = keyboardFrame.size.height;
    
    if (self.userMarks.frame.size.height - keyboardFrame.size.height < 60)
    {
        self.topName.constant = -keyboardFrame.size.height;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark - Publick API methods

+(Class) soulViewModelClass
{
    return [SportDescriptionViewModel class];
}

+(NSString *) soulClassName
{
    return NSStringFromClass([SportDescriptionViewController class]);
}

+(SportDescriptionViewController *) initWithViewModel:(id)viewModel
{
    return [[SportDescriptionViewController alloc] initWithViewModel:viewModel];
}

-(id) soulViewModel
{
    return self.viewModel;
}

@end