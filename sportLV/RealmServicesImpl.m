//
//  RealmServiceImpl.m
//  sportLV
//
//  Created by ios on 21.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealmServicesImpl.h"
#import "JSONSerializable.h"
#import <Realm/Realm.h>
#import <Realm/RLMRealm_Dynamic.h>

@implementation RealmServicesImpl

#pragma mark - Public API methods

-(void) saveJSON:(NSDictionary *)json forClass:(Class<JSONSerializable>)cls
{
    [self saveJSON:json forClass:cls withSaveBlock:nil];
}

-(void) saveJSON:(NSDictionary *)json forClass:(Class<JSONSerializable>)cls withCompletedBlock:(void (^)(void))completedBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    for (NSDictionary *dict in json)
    {
        id obj = [cls fromDictionary:dict];
        [realm addObject:obj];
    }
    [realm commitWriteTransaction];
    if (completedBlock != nil)
    {
        completedBlock();
    }
}

-(void) saveJSON:(NSDictionary *)json forClass:(Class<JSONSerializable>)cls withSaveBlock:(id (^)(id))saveBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    for (NSDictionary *dict in json)
    {
        id obj = [cls fromDictionary:dict];
        if (saveBlock != nil)
        {
            obj = saveBlock(obj);
        }
        [realm addObject:obj];
    }
    [realm commitWriteTransaction];
}


-(NSArray *)getAllObjectForClass:(Class)cls
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *result = [realm allObjects:NSStringFromClass(cls)];
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < result.count; i++)
    {
        [resultArray addObject:[result objectAtIndex:i]];
    }
    return resultArray;
}

-(void) updateObjectWithBlock:(void (^)(void))block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    if (block != nil)
    {
        block();
    }
    [realm commitWriteTransaction];
}

-(id) getObjectFromID:(NSInteger)id forClass:(__unsafe_unretained Class<JSONSerializable>)cls
{
    NSArray *results = [self getAllObjectForClass:cls];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" self.id == %d",id];
    return [results filteredArrayUsingPredicate:predicate].firstObject;
}

-(void) removeAllDatabase
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
}



@end