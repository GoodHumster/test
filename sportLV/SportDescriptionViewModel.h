//
//  SportDescriptionViewModel.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Competition.h"

@protocol ViewModelServices;

@interface SportDescriptionViewModel : NSObject

-(instancetype) initWithServices:(id<ViewModelServices>)services andCompetition:(id)competition;

-(void) updateUserMark:(NSString *)text;

-(id<Competition>) getCompletition;

@property (nonatomic, strong) RACCommand *backCommand;

@end

