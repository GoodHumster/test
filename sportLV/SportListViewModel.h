//
//  SportListViewModel.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@protocol ViewModelServices;

@interface SportListViewModel : NSObject

-(instancetype) initWithServices:(id<ViewModelServices>)services;

@property (nonatomic, strong) RACSignal *fetchedContentSignal;

-(void) reloadData;
-(void) selectedItem:(id)item;

@end