//
//  SportDescriptionViewController.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulBaseModule.h"

@interface SportDescriptionViewController : UIViewController<SoulBaseModule>
@end