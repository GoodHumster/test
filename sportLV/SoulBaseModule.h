//
//  SoulBaseModule.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SoulBaseModule <NSObject>

/* return class registered view model
 */
+(Class) soulViewModelClass;

/* return class name
 */
+(NSString *) soulClassName;

/* intialize resource with view model
 */
+(id<SoulBaseModule>) initWithViewModel:(id)viewModel;

-(id) soulViewModel;

@end