//
//  CoreResourceProvider.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoreResourcesProvider : NSObject

+(CoreResourcesProvider *)sharedInstance;

-(id) provideResourceForViewModel:(id)viewModel;


@end