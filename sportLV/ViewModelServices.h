//
//  ViewModelServices.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiServices.h"
#import "RealmServices.h"

@protocol ViewModelServices <NSObject>

-(void) pushViewModel:(id)viewModel withAnimated:(BOOL)animated;
-(id<ApiServices>) getApiServices;
-(id<RealmServices>) getRealmServices;
-(void) popAnimated:(BOOL)animated;

@end