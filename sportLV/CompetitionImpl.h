//
//  CompetitionImpl.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Competition.h"
#import <Realm/Realm.h>

@interface CompetitionImpl : RLMObject<Competition>
@end