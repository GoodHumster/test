//
//  JSONSerializable.h
//  sportLV
//
//  Created by ios on 20.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@protocol JSONSerializable <NSObject>

- (id<JSONSerializable>)initWithDictionary:(NSDictionary *)dictionary;

+ (id<JSONSerializable>)fromDictionary: (NSDictionary *)dictionary;

- (NSArray *)fromRLMArray:(RLMArray *)rlmArray;

- (id)copy;

@end