//
//  SportListCell.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportListCellView : UITableViewCell

-(void) configWithName:(NSString *)name start:(double)start duration:(double)duration userMark:(NSString *)userMark;

@end