//
//  SportListViewController.m
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SportListViewController.h"
#import "SportListViewModel.h"
#import "SoulRegistratedModule.h"
#import "SportListCellView.h"
#import "Competition.h"

@interface SportListViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *navigationView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) SportListViewModel *viewModel;

@property (strong, nonatomic) NSArray *source;

@end

@implementation SportListViewController

#pragma mark - init methods

+(void) load
{
    [SoulRegistratedModule registeratedClass:[SportListViewController class]];
}

-(instancetype) initWithViewModel:(id)viewModel
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.source.count > 0)
    {
        [self.viewModel reloadData];
    }
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationView.layer.masksToBounds = NO;
    self.navigationView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.25].CGColor;
    self.navigationView.layer.shadowOffset = CGSizeMake(0, 5);
    self.navigationView.layer.shadowOpacity = 0.5;
    self.navigationView.layer.shadowRadius = 1;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"SportListViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cellID"];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.estimatedRowHeight = 500;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self bindViewModel];
}

-(void) bindViewModel
{
    [self.viewModel.fetchedContentSignal subscribeNext:^(id value) {
        self.source = value;
        [self.tableView reloadData];
    }];
}

#pragma mark table view delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SportListCellView *cellView = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    id<Competition> competion = self.source[indexPath.row];
    
    [cellView configWithName:[competion name] start:[competion startInSeconds] duration:[competion durationInSeconds] userMark:[competion userMark]];
    
    
    return cellView;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.viewModel selectedItem:self.source[indexPath.row]]; 
}

#pragma mark table view data source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.source.count;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - Publick API methods

+(Class) soulViewModelClass
{
    return [SportListViewModel class];
}

+(NSString *) soulClassName
{
    return NSStringFromClass([SportListViewController class]);
}

+(SportListViewController *) initWithViewModel:(id)viewModel
{
    return [[SportListViewController alloc] initWithViewModel:viewModel];
}


-(id) soulViewModel
{
    return self.viewModel;
}

@end