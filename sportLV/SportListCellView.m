//
//  SportListView.m
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SportListCellView.h"

@interface SportListCellView ()

@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *userMark;

@end

@implementation SportListCellView

-(void) configWithName:(NSString *)name start:(double)start duration:(double)duration userMark:(NSString *)userMark
{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:start];
    NSDateFormatter *dateFormmatter = [[NSDateFormatter alloc] init];
    dateFormmatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    self.name.text = name;
    self.userMark.text = userMark;
    self.startTime.text = [dateFormmatter stringFromDate:date];
    self.duration.text = [NSString stringWithFormat:@"%d",(int)(duration/60)];
    [self.userMark sizeToFit];
}

@end