//
//  SportDescriptionViewModel.m
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SportDescriptionViewModel.h"
#import "ViewModelServices.h"
#import "CompetitionImpl.h"

@interface SportDescriptionViewModel ()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, strong) CompetitionImpl *competition;


@end

@implementation SportDescriptionViewModel

-(instancetype) initWithServices:(id<ViewModelServices>)services andCompetition:(id)competition
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.services = services;
    self.competition = competition;
    [self initialized];
    return self;
}

-(void) initialized
{
    __weak typeof(self) weakSelf = self;
    self.backCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            __strong typeof(weakSelf) blockSelf = weakSelf;
            [blockSelf.services popAnimated:YES];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
}

-(void) updateUserMark:(NSString *)text
{
    __weak typeof(self) weakSelf = self;
    [[self.services getRealmServices] updateObjectWithBlock:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        blockSelf.competition.userMark = text;
    }];
}

-(id<Competition>) getCompletition
{
    return self.competition;
}

@end