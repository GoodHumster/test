//
//  ApiServices.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@protocol ApiServices <NSObject>

-(RACSignal *) fetchSportContents;

@end