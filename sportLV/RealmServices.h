//
//  RealmServices.h
//  sportLV
//
//  Created by ios on 21.04.16.
//  Copyright © 2016 MSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONSerializable.h"

@protocol RealmServices <NSObject>

-(void) saveJSON:(NSDictionary *)json forClass:(Class<JSONSerializable>)cls;

-(void) removeAllDatabase;

-(void) saveJSON:(NSDictionary *)json  forClass:(Class<JSONSerializable>)cls withSaveBlock:(id(^)(id))saveBlock;

-(void) saveJSON:(NSDictionary *)json forClass:(Class<JSONSerializable>)cls withCompletedBlock:(void(^)(void))completedBlock;

-(NSArray *)getAllObjectForClass:(Class)cls;

-(id) getObjectFromID:(NSInteger)id forClass:(Class<JSONSerializable>)cls;

-(void) updateObjectWithBlock:(void(^)(void))block;

@end