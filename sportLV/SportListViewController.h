//
//  SportListViewController.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulBaseModule.h"

@interface SportListViewController : UIViewController<SoulBaseModule>

@end