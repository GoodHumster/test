//
//  SportListViewModel.m
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SportListViewModel.h"
#import "ViewModelServices.h"
#import "CompetitionImpl.h"
#import "SportDescriptionViewModel.h"

@interface SportListViewModel ()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, weak) NSArray *contents;


@end

@implementation SportListViewModel

#pragma mark - initialized

-(instancetype) initWithServices:(id<ViewModelServices>)services
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.services = services;
    [self initialized];
    return self;
}

-(void) initialized
{
    RACSignal *contentSignal = [[self.services getApiServices] fetchSportContents];
    
    self.fetchedContentSignal = [RACObserve(self, contents) skip:1];
    
    __weak typeof(self) weakSelf = self;
    [contentSignal subscribeNext:^(id value) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [[blockSelf.services getRealmServices] saveJSON:value[@"result"] forClass:[CompetitionImpl class] withCompletedBlock:^{
            NSArray *mutable = [[blockSelf.services getRealmServices] getAllObjectForClass:[CompetitionImpl class]];
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startInSeconds" ascending:NO];
            blockSelf.contents = [mutable sortedArrayUsingDescriptors:@[sortDescriptor]];
        }];
    }];
}

#pragma mark - Publick API methods

-(void) reloadData
{
    NSArray *mutable = [[self.services getRealmServices] getAllObjectForClass:[CompetitionImpl class]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startInSeconds" ascending:NO];
    self.contents = [mutable sortedArrayUsingDescriptors:@[sortDescriptor]];
}

-(void) selectedItem:(id)item
{
    SportDescriptionViewModel *viewModel = [[SportDescriptionViewModel alloc] initWithServices:self.services andCompetition:item];
    [self.services pushViewModel:viewModel withAnimated:YES];
}

@end