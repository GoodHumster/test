//
//  CompetitionImpl.m
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompetitionImpl.h"

@implementation CompetitionImpl

@synthesize id;
@synthesize startInSeconds;
@synthesize durationInSeconds;
@synthesize relativeInSeconds;
@synthesize name;
@synthesize userMark;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    
    self.id = [dictionary[@"id"] integerValue];
    self.name = dictionary[@"name"];
    self.relativeInSeconds = [dictionary[@"relativeTimeSeconds"] doubleValue];
    self.durationInSeconds = [dictionary[@"durationSeconds"] doubleValue];
    self.startInSeconds = [dictionary[@"startTimeSeconds"] doubleValue];
    
    return self;
}

+(CompetitionImpl *)fromDictionary: (NSDictionary *)dictionary
{
    return [[CompetitionImpl alloc] initWithDictionary:dictionary];
}

-(NSArray *) fromRLMArray:(RLMArray *)rlmArray
{
    NSMutableArray *mutable = [[NSMutableArray alloc] init];
    for (NSObject *object in rlmArray)
    {
        [mutable addObject:object];
    }
    return mutable;
}

@end