//
//  ViewModelServicesImpl.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewModelServices.h"

@interface ViewModelServicesImpl : NSObject<ViewModelServices>

-(instancetype)initWithNavigationController:(id)navigationController;

@end