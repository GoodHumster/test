//
//  SoulRegistratedModule.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoulRegistratedModule: NSObject

+(void) registeratedClass:(Class) registratedClass;

+(NSArray *) classArray;

@end

extern NSString* const kCoreModuleRegistratedClassKey;