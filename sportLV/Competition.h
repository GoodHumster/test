//
//  Competition.h
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONSerializable.h"

@protocol Competition <NSObject,JSONSerializable>

@property (nonatomic) NSInteger id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) double durationInSeconds;
@property (nonatomic) double startInSeconds;
@property (nonatomic) double relativeInSeconds;
@property (nonatomic, strong) NSString *userMark;

@end