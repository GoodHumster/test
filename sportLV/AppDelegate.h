//
//  AppDelegate.h
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

