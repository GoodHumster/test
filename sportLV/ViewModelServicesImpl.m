//
//  ViewModelServicesImpl.m
//  sportLV
//
//  Created by ios on 12.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewModelServicesImpl.h"
#import "CoreResourcesProvider.h"
#import "SportListViewModel.h"
#import "SportDescriptionViewModel.h"
#import "ApiServicesImpl.h"
#import "RealmServicesImpl.h"

@interface ViewModelServicesImpl ()

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) ApiServicesImpl *apiServices;
@property (nonatomic, strong) RealmServicesImpl *realmServices;

@end

@implementation ViewModelServicesImpl

#pragma mark - instance

-(instancetype) initWithNavigationController:(id)navigationController
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.navigationController = navigationController;
    [self initialized];
    return self;
}

-(void) initialized
{
    self.apiServices = [[ApiServicesImpl alloc] init];
    self.realmServices = [[RealmServicesImpl alloc] init];
    
    SportListViewModel *viewModel = [[SportListViewModel alloc] initWithServices:self];
    [self pushViewModel:viewModel withAnimated:YES];
}

#pragma mark - Publick API methods

-(void) pushViewModel:(id)viewModel withAnimated:(BOOL)animated
{
    CoreResourcesProvider *resourceProvider = [CoreResourcesProvider sharedInstance];
    UIViewController *viewController = [resourceProvider provideResourceForViewModel:viewModel];
    [self pushViewController:viewController animated:animated];
}

-(void) popAnimated:(BOOL)animated
{
    [self.navigationController popViewControllerAnimated:animated];
}

-(id<ApiServices>) getApiServices
{
    return self.apiServices;
}

-(id<RealmServices>)getRealmServices
{
    return self.realmServices;
}

#pragma mark - utilites methods

-(void) pushViewController:(UIViewController *)vc animated:(BOOL)animated
{
    self.navigationController.delegate = nil;
    [self.navigationController pushViewController:vc animated:animated];
}

@end