//
//  ApiServicesImpl.m
//  sportLV
//
//  Created by ios on 13.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ApiServicesImpl.h"

NSString *const HOST = @"http://codeforces.com";
NSString *const CONTENT = @"/api/contest.list";

@interface ApiServicesImpl ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *operationManager;

@end

@implementation ApiServicesImpl

#pragma mark - init methods

-(instancetype) init
{
    self.operationManager = [[AFHTTPRequestOperationManager alloc] init];
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    return self;
}

#pragma mark - Publick API methods

-(RACSignal *) fetchSportContents
{
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,CONTENT];
    return [self rac_GET:url parametrs:nil];
}

#pragma mark - utilites methods

-(RACSignal *)rac_POST:(NSString *)urlString parametrs:(NSDictionary *)paramaters
{
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        AFHTTPRequestOperation *op = [self.operationManager POST:urlString parameters:paramaters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
            NSLog(@"Success: %@",operation);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSMutableDictionary *userInfo = [error.userInfo mutableCopy] ?: [NSMutableDictionary dictionary];
            //    userInfo[kAFNetworkingReactiveExtensionErrorInfoOperationKey] = operation;
            NSError *errorWithOperation = [NSError errorWithDomain:error.domain code:error.code userInfo:userInfo];
            NSLog(@"%@ Error: %@",operation,error);
            [subscriber sendError:errorWithOperation];
        }];
        return [RACDisposable disposableWithBlock:^{
            [op cancel];
        }];
    }] replayLazily];
}

-(RACSignal *)rac_GET:(NSString *)urlString parametrs:(NSDictionary *)parametrs
{
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        AFHTTPRequestOperation *op = [self.operationManager GET:urlString parameters:parametrs success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
            NSLog(@"%@",self.operationManager.requestSerializer.HTTPRequestHeaders);
            NSLog(@"Success: %@",operation);
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
            NSMutableDictionary *userInfo = [error.userInfo mutableCopy] ?: [NSMutableDictionary dictionary];
            //    userInfo[kAFNetworkingReactiveExtensionErrorInfoOperationKey] = operation;
            NSError *errorWithOperation = [NSError errorWithDomain:error.domain code:error.code userInfo:userInfo];
            NSLog(@"%@ Error: %@",operation,error);
            [subscriber sendError:errorWithOperation];
        }];
        return [RACDisposable disposableWithBlock:^{
            [op cancel];
        }];
    }] replayLazily];
}

@end